
import {useNavigation, useRoute} from '@react-navigation/core'
import {observer} from 'mobx-react-lite'
import React from 'react'
import {Dimensions, Image, Pressable, StyleSheet} from 'react-native'

const GIF_WIDTH = (Dimensions.get('window').width * 0.47)

export default observer((gif) => {
  const navigation = useNavigation()
  const route = useRoute()
  return (
    <Pressable
      style={styles.gifItemContainer}
      onPress={() => {
        if (route.name === 'details') {
          navigation.replace('details', gif)
        } else {
          navigation.navigate('details', gif)
        }
      }}
    >
      <Image
        source={{uri: gif?.gif.link}}
        alt="gif"
        resizeMode="cover"
        style={{
          width: GIF_WIDTH,
          height: GIF_WIDTH
        }}
      />
    </Pressable>
  )
})

const styles = StyleSheet.create({
  gifItemContainer: {
    marginTop: 8,
    overflow: 'hidden',
    borderRadius: 16,
  },
})